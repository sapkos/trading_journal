FROM python:3.8.0-alpine

WORKDIR /usr/src/app

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

RUN pip install --upgrade pip

COPY requirements/requirements.txt .

RUN pip install -r requirements.txt

COPY src/trading_journal /usr/src/app

ENTRYPOINT ["./entrypoint.sh"]

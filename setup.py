from setuptools import find_packages
from setuptools import setup

with open('requirements/requirements.txt') as f:
    requirements = f.read().splitlines()

with open('requirements/requirements-dev.txt') as f:
    requirements_dev = f.read().splitlines()

setup(
    name="trading_journal",
    version="0.1.0",
    package_dir={'': 'src/trading_journal'},
    packages=find_packages(where='src/trading_journal'),
    extras_require={
        'dev': requirements_dev
    },
    install_requires=requirements,
    include_package_data=True,
)

#!/bin/bash

HOOK_DIR=$(git rev-parse --show-toplevel)/.git/hooks
git config core.hooksPath $HOOK_DIR

ln -f ops/hooks/pre-commit .git/hooks/pre-commit

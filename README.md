## Trading Journal

Efficient trading journal for energy oriented markets!

The app has configuration for two environments - staging and production. 

Current setup starts postgres in seperate container with shared volume.
This should be changed in production environment.

To run staging environment: 
```
make start-staging
```
Website will be available at `localhost:8000`

To bring it down:
```
make stop-staging
```

App has some tests to test basic functionalities, to run tests do:
```
make tests
```

Similarly to run/stop production environment:
```
make start-prod
make stop-prod
```

This will serve application from gunicorn behind nginx reverse proxy.
Nginx configuration can be found in `./nginx/nginx.conf`

Production/staging environmnets are defined in `.env` files.

For development run `make fresh-dev` it will create virtual environment and configure git hooks.
Activate virtualenv with `source env/bin/activate`

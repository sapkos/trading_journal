import django_tables2 as tables
from .models import Journal


class JournalTable(tables.Table):
    id = tables.Column(linkify=True)
    ts = tables.DateTimeColumn(format="d/m/Y", linkify=True)

    class Meta:
        model = Journal
        attrs = {"class": "table table-hover table-striped"}
        template_name = "django_tables2/bootstrap.html"
        fields = ("ts", "content", "product",
                  "delivery_period", "mark_to_market_prev_product",
                  "mental", "retrospective")
        sequence = ("id", "ts", "product", "delivery_period",
                    "mark_to_market_prev_product", "content",
                    "mental", "retrospective")

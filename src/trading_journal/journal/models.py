from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

# Create your models here.


class Product(models.Model):
    PRODUCT_CHOICES = [
        ("DA", "Daily"),
        ("WEEK", "Weekly"),
        ("MONTH", "Monthly"),
        ("QUARTER", "Quarterly"),
        ("YEAR", "Yearly")
    ]

    COUNTRY_CHOICES = [
        ("PL", "Poland"),
        ("GER", "Germany"),
        ("IT", "Italy"),
        ("HU", "Hungary")
    ]

    country = models.CharField(max_length=20, choices=COUNTRY_CHOICES, default="PL")
    product_type = models.CharField(max_length=8, choices=PRODUCT_CHOICES, default="DA")

    def __str__(self):
        return f"{self.get_country_display()} {self.get_product_type_display()}"


class Journal(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    ts = models.DateTimeField()
    fair_price = models.TextField(blank=True, null=True)
    content = models.TextField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    delivery_period = models.TextField(default="", max_length=30)
    mark_to_market_prev_product = models.FloatField(null=True, verbose_name="mtm_pi")
    decision = models.TextField(blank=True)
    confidence = models.IntegerField(blank=True)
    mental = models.TextField(blank=True)
    execution = models.TextField(blank=True)
    result = models.FloatField(blank=True, null=True)
    greed_index = models.IntegerField(blank=True)
    retrospective = models.TextField(blank=True)

    def __str__(self):
        return f"{self.author.get_username()} on {self.ts} in {self.product}"

    def get_absolute_url(self):
        return reverse('journal-update', args=[self.id, ])


class Trade(models.Model):
    ts = models.DateTimeField('trade_timestamp')
    id_user = models.IntegerField()
    book = models.IntegerField()
    price = models.FloatField()
    quantity = models.FloatField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    delivery_date_start = models.DateTimeField()
    delivery_date_end = models.DateTimeField()
    journal = models.ForeignKey(Journal, on_delete=models.CASCADE)

    def __str__(self):
        return f"Trade done by {self.id_user} in {self.ts}"

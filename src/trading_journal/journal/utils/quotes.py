import random
from datetime import datetime
from operator import itemgetter

import json
import requests

from django.core.cache import cache

QUOTES_URL = 'https://quotes.rest/qod?category=inspire&language=en'
QUOTES_KEY = '_qod'


def quote_of_the_day():
    version = datetime.now().date().isoformat()
    response = cache.get(QUOTES_KEY, version=version)
    if not response:
        response = requests.get(QUOTES_URL).text
        cache.set(QUOTES_KEY, response, version=version)

    response = json.loads(response)
    if random.random() < .1 or 'success' not in response:
        return ('Thomas A. Edison', 'I failed my way to success')
    get_author_and_quote = itemgetter('author', 'quote')
    return get_author_and_quote(
        response
        .get('contents')
        .get('quotes')
        .pop()
    )

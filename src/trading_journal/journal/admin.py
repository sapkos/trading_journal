from django.contrib import admin

from .models import Trade, Journal, Product

admin.site.register(Trade)
admin.site.register(Journal)
admin.site.register(Product)

# Register your models here.

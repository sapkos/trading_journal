import django_filters
from django_filters import DateFromToRangeFilter, ChoiceFilter
from django_filters.widgets import RangeWidget

from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Layout, Row, Column, Submit

from .models import Journal, Product


class JournalFilter(django_filters.FilterSet):
    ts = DateFromToRangeFilter(
        label='Browse entries from:',
        widget=RangeWidget(
            attrs={
                'type': 'date'
                }
            )
        )

    product__product_type = ChoiceFilter(
        label='Product type',
        choices=Product.PRODUCT_CHOICES
    )

    class Meta:
        model = Journal
        fields = {
            'product__product_type': ['exact'],
            'product__country': ['exact'],
            'ts': ['exact'],
        }

    @property
    def qs(self):
        parent = super().qs
        author = getattr(self.request, 'user', None)

        if author.username == 'admin':
            return parent

        return parent.filter(author=author)

    @property
    def helper(self):
        if not hasattr(self, '_helper'):
            self._helper = FormHelper()
            self._helper.form_method = 'get'
            self._helper.form_class = 'form-group'
            self._helper.layout = Layout(
                Row(
                    Column('ts')
                ),
                Row(
                    Column('product__product_type'),
                    Column('product__country'),
                ),
                FormActions(
                    Submit('submit', 'Filter')
                )
            )
        return self._helper

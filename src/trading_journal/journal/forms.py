from django.forms import ModelForm
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Layout, Submit, Row, Column, HTML
from journal.models import Journal


class DateInput(forms.DateInput):
    input_type = 'date'


class JournalForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Row(
                Column('ts', css_class='form-group col-md-4 mb-0'),
                Column('product', css_class='form-group col-md-4 mb-0'),
                Column('delivery_period', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('mark_to_market_prev_product', css_class='form-group col-md-4 mb-0'),
                Column('greed_index', css_class='form-group col-md-4 mb-0'),
                Column('confidence', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('fair_price', css_class='form-group col-md-4 mb-0'),
                Column('content', css_class='form-group col-md-8 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('decision', css_class='form-group col-md-2 mb-0'),
                Column('mental', css_class='form-group col-md-5 mb-0'),
                Column('execution', css_class='form-group col-md-5 mb-0'),
                css_class='form-row'
            ),
            'retrospective',
            FormActions(
                Submit('submit', 'Add entry'),
                HTML("""{% if journal.id %}\
                        <a href="{% url "journal-delete" journal.id %}" class="btn btn-danger"\
                        onclick="return confirm('Are you sure?');">Delete</a>\
                        {% endif %}"""),
            )
        )

    class Meta:
        model = Journal
        fields = ['ts',
                  'product', 'delivery_period',
                  'mark_to_market_prev_product',
                  'content', 'mental',
                  'decision', 'execution',
                  'confidence', 'greed_index',
                  'retrospective', 'fair_price']
        widgets = {
            'ts': DateInput(),
            'delivery_date_start': DateInput(),
            'delivery_date_end': DateInput(),
            'delivery_period': forms.Textarea(attrs={'rows': 1}),
            'decision': forms.Textarea(attrs={'rows': 4}),
            'fair_price': forms.Textarea(attrs={'rows': 7}),
            'content': forms.Textarea(attrs={'rows': 7}),
            'mental':  forms.Textarea(attrs={'rows': 4}),
            'execution':  forms.Textarea(attrs={'rows': 4}),
            'retrospective':  forms.Textarea(attrs={'rows': 3}),
        }

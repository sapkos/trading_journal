import datetime as dt

from django.urls import reverse_lazy
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

from journal.utils import quotes

from .forms import JournalForm
from .tables import JournalTable
from .models import Journal
from .filters import JournalFilter


class JournalCreate(LoginRequiredMixin, CreateView):
    form_class = JournalForm
    model = Journal
    success_url = reverse_lazy('journal-list')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_initial(self):
        return {"ts": dt.date.today()}


class JournalUpdate(LoginRequiredMixin, UpdateView):
    form_class = JournalForm
    model = Journal
    success_url = reverse_lazy('journal-list')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class JournalDelete(LoginRequiredMixin, DeleteView):
    model = Journal
    success_url = reverse_lazy('journal-list')

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)


class JournalDetailView(LoginRequiredMixin, DetailView):
    model = Journal
    template_name = "journal/journal_detail.html"


class JournalList(LoginRequiredMixin, SingleTableMixin, FilterView):
    model = Journal
    table_class = JournalTable
    template_name = "journal/journal_list.html"
    ordering = ['-ts']
    filterset_class = JournalFilter


def home(request):
    author, quote = quotes.quote_of_the_day()
    return render(
        request,
        'journal/index.html',
        {
            'title': 'Home page',
            'author': author,
            'quote': quote,
        }
    )

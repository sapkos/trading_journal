from django.urls import reverse
from django.forms.models import model_to_dict
from django.test import TestCase, RequestFactory

from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.middleware import SessionMiddleware

from journal.views import JournalCreate, JournalUpdate, JournalList
from journal.tests.factory import JournalFactory, UserFactory


def add_middleware_to_request(request, middleware_class):
    middleware = middleware_class()
    middleware.process_request(request)
    return request


def add_middleware_to_response(request, middleware_class):
    middleware = middleware_class()
    middleware.process_response(request)
    return request


class JournalCreateTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory()

    def test_add_journal_view(self):
        request = self.factory.get(reverse('journal-add'))
        request.user = AnonymousUser()

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalCreate.as_view()(request)

        # unlogged users should be pointed to login page
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/accounts/login/?next=/add_journal/')

        self.client.force_login(self.user)
        request.user = self.user

        response = JournalCreate.as_view()(request)

        # once we login we should receive website
        self.assertEqual(response.status_code, 200)


class JournalUpdateTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory()
        self.journal = JournalFactory(author=self.user)

    def test_journal_update_view(self):
        request = self.factory.get(reverse('journal-update',
                                           kwargs={'pk': self.journal.pk}))
        request.user = self.user

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalUpdate.as_view()(request, pk=self.journal.pk)

        # should receive 200 OK
        self.assertEqual(response.status_code, 200)
        # should have correct journal
        self.assertEqual(response.context_data['journal'], self.journal)

        journal_dict = model_to_dict(self.journal)
        journal_dict['content'] = 'has changed'

        # change the journal
        request = self.factory.post(reverse('journal-update',
                                            kwargs={'pk': self.journal.pk}),
                                    data=journal_dict)

        request.user = self.user

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalUpdate.as_view()(request, pk=self.journal.pk)

        # correctly filled form should redirect us
        self.assertEqual(response.status_code, 302)

        self.journal.refresh_from_db()

        # updated object should change
        self.assertEqual(self.journal.content, 'has changed')


class JournalListTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = UserFactory()

    def test_list_view(self):
        request = self.factory.get(reverse('journal-list'))
        request.user = self.user

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalList.as_view()(request)

        self.assertEqual(response.status_code, 200)
        listed_objects_size = len(response.context_data['object_list'])

        # new user dont have any entries
        self.assertEqual(listed_objects_size, 0)

        # create some new entries
        JournalFactory.create_batch(5, author=self.user)

        request = self.factory.get(reverse('journal-list'))
        request.user = self.user

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalList.as_view()(request)

        self.assertEqual(response.status_code, 200)
        listed_objects_size = len(response.context_data['object_list'])

        # new entries should appear here
        self.assertEqual(listed_objects_size, 5)

    def test_list_filter(self):
        request = self.factory.get(reverse('journal-list'))
        request.user = self.user

        JournalFactory.create_batch(5, author=self.user, product__country='GER')
        JournalFactory.create_batch(3, author=self.user, product__country='IT')

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalList.as_view()(request)
        pre_filter_objects_size = len(response.context_data['object_list'])

        # Before filtering users sees all objects
        self.assertEqual(pre_filter_objects_size, 8)

        request = self.factory.get(reverse('journal-list'),
                                   {
                                       'product__country': 'GER',
                                       'submit': 'Filter'
                                   })
        request.user = self.user

        request = add_middleware_to_request(request, SessionMiddleware)
        request.session.save()

        response = JournalList.as_view()(request)

        post_filter_objects_size = len(response.context_data['object_list'])

        self.assertEqual(post_filter_objects_size, 5)

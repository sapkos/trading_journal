from operator import itemgetter

from django.contrib.auth.models import User

import factory
import factory.fuzzy  # pylint: disable=E0401, E0611

from journal.models import Journal, Product


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product
        django_get_or_create = ('country', 'product_type')

    country = factory.fuzzy.FuzzyChoice(Product.COUNTRY_CHOICES, getter=itemgetter(0))
    product_type = factory.fuzzy.FuzzyChoice(Product.PRODUCT_CHOICES, getter=itemgetter(0))


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('username', )

    username = factory.Faker('first_name')


class JournalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Journal

    author = factory.SubFactory(UserFactory)
    ts = factory.Faker('past_datetime', start_date='-5d')
    fair_price = factory.Faker('sentence')
    content = factory.Faker('paragraph')
    product = factory.SubFactory(ProductFactory)
    delivery_period = factory.Faker('day_of_week')
    mark_to_market_prev_product = factory.Faker('pyfloat', min_value=0, max_value=1)
    decision = factory.Faker('sentence')
    confidence = factory.Faker('pyint', min_value=0, max_value=10)
    mental = factory.Faker('paragraph')
    execution = factory.Faker('paragraph')
    result = factory.Faker('pyfloat', min_value=0, max_value=1)
    greed_index = factory.Faker('pyint', min_value=0, max_value=5)
    retrospective = factory.Faker('paragraph')

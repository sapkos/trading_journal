from django.urls import path
from journal.views import JournalDetailView, JournalList, JournalCreate, JournalUpdate, JournalDelete
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('update_journal/<int:pk>/', JournalUpdate.as_view(), name="journal-update"),
    path('delete_journal/<int:pk>/', JournalDelete.as_view(), name="journal-delete"),
    path('add_journal/', JournalCreate.as_view(), name="journal-add"),
    path('<int:pk>/', JournalDetailView.as_view(), name='journal-detail'),
    path('journals/', JournalList.as_view(), name="journal-list"),
]

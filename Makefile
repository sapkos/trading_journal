.PHONY: env

clean-py:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	rm -rf env

git-hooks:
	ops/scripts/configure_hooks.sh

env:
	python3.6 -m venv env
	env/bin/pip install --upgrade pip
	env/bin/pip install -e .[dev]

fresh-dev: clean-py env git-hooks

lint:
	flake8 --exit-zero src

start-staging:
	docker-compose up -d --build
	docker-compose exec web python manage.py createcachetable

stop-staging:
	docker-compose down

tests: start-staging
	docker-compose exec web python manage.py test
	make stop-staging

start-prod:
	docker-compose -f docker-compose.prod.yml up -d --build
	docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear
	docker-compose -f docker-compose.prod.yml exec web python manage.py createcachetable

stop-prod:
	docker-compose -f docker-compose.prod.yml down

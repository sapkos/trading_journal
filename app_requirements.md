# Base requirements for first version of app:
1. - [ ] Adding new journal entries with options to include:
  * Mark to market price of previous iteration of the same instrument,    
  * Price index, 
  * Description of the trade, dividied into few sections, supporting markdown (fundaments, execution, mental),  
  * Delivery date - starting to end, or just a name,  
  * Trading day - default today, but can be altered,  
  * Decision for this instrument,
  * Confidence,
  * Market greed or fear index,
  * Tag,
  * After results analysis field,  
2. - [ ] Browsing previous journal entries:
  * Sorting by tag, price index, or trading day or price index.
  * After each instrument finished add them to after analysis section in journal.
  * Ability to change previous entries.
3. - [ ] Users:
  * The app has to be able to contain few users, each of one has its own trades. There should be also account for person to 
      see everybody's journal.
4. - [ ] Import trades:
  * User should be able to import trade from eex via csv, and this trades should be associated with certain entries in journal, 
  connected via price index and delivery date. 
5. - [ ] Profit and loss calculation:
  * The app should be able to calculate pnl for each trader indvidually based on trades he imported.
6. - [ ] Analysis:
  * The app should contain reports about user trading containg pnl, win ratio(for shorts and longs), ability to group by type of statsics grouping by 
  instruments, and price indexes. Also there should be weekly report containg each trade with journal entries.
7. - [ ] Position display:
  * The app should be able to display net position for cetrain country for certain day, so that user can see what is his and company position
      net for certain day or date-range(if date-range display min). Also there should be an option to select all trades which has 
       this date in delivery range. 
